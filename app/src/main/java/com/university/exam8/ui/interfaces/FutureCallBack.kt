package com.university.exam8.ui.interfaces

import com.university.exam8.bean.NewsModel

interface FutureCallBack<T> {
    fun done(result: String) {}
    fun error(title: String, errorMessage: String) {}
}
